## Basic setup

1. [ ] Read the [code review page in the handbook](https://about.gitlab.com/handbook/engineering/workflow/code-review/) and the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
2. [ ] Add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer) on the team page.
3. [ ] Assign yourself and your buddy to this issue, and set up a call / Slack conversation between the two of you.

## Working towards becoming a maintainer

There is no checklist here, only guidelines. Remember that there is no specific timeline on this.

Ensure that you are getting enough MRs to review, and of varied types. It is helpful to add a discussion to this issue for each MR there, for any conversations you and your buddy want to have about the review itself, without cluttering the MR.

## When you're ready to make it official

1. [ ] [Create a merge request proposing yourself as a maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer)
2. [ ] Ask your buddy to add you to [gitlab-org/maintainers/rails-backend](https://gitlab.com/gitlab-org/maintainers/rails-backend)
3. [ ] Keep reviewing, start merging :metal:

/label ~"trainee maintainer"
