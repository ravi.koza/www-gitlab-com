---
layout: markdown_page
title: "Merge Request Coach"
---

The main goal of a Merge Request Coach is to help
[merge requests from the community](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?label_name[]=Community%20contribution)
get merged into GitLab.

## Responsibilities

* Triage merge requests labeled `~Community Contribution`.
* Close merge requests that we don't want, with a clear explanation on the
  reasons why, so that people don't feel discouraged.
* Help contributors to get their merge requests to meet the
  [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria).
* If the contributor is unresponsive or if they are unable to finish it, finish
  their merge requests.
  1. Close the original merge request and say that you will finish it.
  1. Check out the branch locally.
  1. Keep at least one original commit (you can squash commits if there are a lot)
     from the author.
  1. Make sure a changelog entry crediting the author exists.
  1. Add your own commits to improve and finish the original work.
  1. Push to a new branch and open a new merge request.
* Make it easy to contribute to GitLab even for people who are new to Ruby,
  JavaScript, or programming entirely.

## Apply

1. Create an MR to add "Merge Request coach" to your team page entry.
1. Explain your motivation in the MR body:
   * Why you want to become a Merge Request coach?
   * How much time you are planning to spend on it?
   * Which duties you are focusing on (e.g. triage, finish stale MRs)?
1. Mention `@gitlab-org/coaches` and assign one of them.
